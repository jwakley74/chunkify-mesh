bl_info = {
    "name": "Chunkify Mesh",
    "description": "",
    "author": "h4v0c",
    "blender": (2, 80, 0),
    "version": (0, 0, 1),
    "location": "3D View > Tool > Chunkify Mesh",
    "warning": "",  # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Mesh"
}


import bpy
import bmesh
from bpy.types import (Panel, Operator, PropertyGroup)
from bpy.props import (IntProperty, PointerProperty)
from mathutils import Vector

# Code adapted from batFINGER at: https://blender.stackexchange.com/questions/80460/slice-up-terrain-mesh-into-chunks

def bbox(ob):
    return (Vector(b) for b in ob.bound_box)


def bbox_center(ob):
    return sum(bbox(ob), Vector()) / 8


def bbox_axes(ob):
    bb = list(bbox(ob))
    return tuple(bb[i] for i in (0, 4, 3, 1))


def slice(bm, start, end, segments):
    if segments == 1:
        return

    def geom(bm):
        return bm.verts[:] + bm.edges[:] + bm.faces[:]
    planes = [start.lerp(end, f / segments) for f in range(1, segments)]
    #p0 = start
    plane_no = (end - start).normalized()
    while(planes):
        p0 = planes.pop(0)
        ret = bmesh.ops.bisect_plane(bm,
                                     geom=geom(bm),
                                     plane_co=p0,
                                     plane_no=plane_no)
        bmesh.ops.split_edges(bm,
                              edges=[e for e in ret['geom_cut']
                                     if isinstance(e, bmesh.types.BMEdge)])


def chunkify(context):
    chunkify = context.scene.chunkify

    print("Chunkify Mesh:")
    print("x_segments:", chunkify.x_segments)
    print("y_segments:", chunkify.y_segments)
    print("z_segments:", chunkify.z_segments)

    bm = bmesh.new()
    ob = context.object
    me = ob.data
    bm.from_mesh(me)

    o, x, y, z = bbox_axes(ob)

    x_segments = chunkify.x_segments
    y_segments = chunkify.y_segments
    z_segments = chunkify.z_segments

    slice(bm, o, x, x_segments)
    slice(bm, o, y, y_segments)
    slice(bm, o, z, z_segments)
    bm.to_mesh(me)

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.separate(type='LOOSE')
    bpy.ops.object.mode_set()


class ChunkifyProps(PropertyGroup):
    x_segments: IntProperty(
        name="X Segments",
        description="",
        default=2,
        min=1
    )

    y_segments: IntProperty(
        name="Y Segments",
        description="",
        default=2,
        min=1
    )

    z_segments: IntProperty(
        name="Z Segments",
        description="",
        default=1,
        min=1
    )


class WM_OT_Chunkify(Operator):
    bl_label = "Chunkify Mesh"
    bl_idname = "wm.chunkify"

    def execute(self, context):
        chunkify(context)
        return {'FINISHED'}


class OBJECT_PT_ChunkifyPanel(Panel):
    bl_label = "Chunkify Mesh"
    bl_idname = "OBJECT_PT_ChunkifyPanel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tool"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        chunkify = scene.chunkify

        layout.prop(chunkify, "x_segments")
        layout.prop(chunkify, "y_segments")
        layout.prop(chunkify, "z_segments")
        layout.separator()
        layout.operator("wm.chunkify")


classes = (
    ChunkifyProps,
    WM_OT_Chunkify,
    OBJECT_PT_ChunkifyPanel
)


def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.chunkify = PointerProperty(type=ChunkifyProps)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.chunkify


if __name__ == "__main__":
    register()
